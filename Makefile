codestyle:
	ruff check --fix
	ruff format

build:
	docker stop task1 || true
	docker rm task1 || true
	docker build . -t task1
	docker create --name task1 \
		--mount type=bind,source="$(shell pwd)",target=/code \
		-u $(shell id -u ${USER}):$(shell id -g ${USER}) \
		-it task1

remove:
	docker stop task1
	docker rm task1

shell:
	docker start task1
	docker exec -it task1 /bin/bash

stop:
	docker stop task1