FROM python:3.11-slim
WORKDIR /code
RUN pip install poetry
COPY ./poetry.lock /code/poetry.lock
COPY ./pyproject.toml /pyproject.toml
RUN poetry install --with dev
CMD ["/bin/bash"]